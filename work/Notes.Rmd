---
title: "Notes mvordflex"
author: "Laura Vana"
date: "2022-11-24"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```


# Unconditional variance derivations

## AR(1) process with standard normal errors (probit link) 

The variance of $\boldsymbol\epsilon_t$ is given by
$$
V(\boldsymbol\epsilon_t) = \Psi V(\boldsymbol\epsilon_{t-1})\Psi^\top + \Sigma
$$
If stationary, then it must hold that $V(\epsilon_t) =V(\epsilon_{t-1}) = \tilde\Sigma$
So we can find $\Sigma_0$ by 
$$
vec(\tilde\Sigma)=(I-\Psi \otimes \Psi)^{-1}vec(\Sigma)
$$
For the covariance we have (these will go to the lower triangle of the large variance-covariance matrix $\Sigma^*$):
$$
COV(\epsilon_t, \epsilon_{t-1}) = COV(\Psi\epsilon_{t-1} + \Sigma^{1/2}u_t, \epsilon_{t-1}) = \Psi \tilde\Sigma
$$
and  (these will go to the upper triangle of the large variance-covariance matrix)
$$
COV(\epsilon_{t-1}, \epsilon_{t}) = COV(\epsilon_{t-1}, \Psi\epsilon_{t-1}+ \Sigma^{1/2}u_{t}) = \tilde\Sigma\Psi^\top
$$

The covariance of the $qT$ dimensional vector on a subject-level in mvordflex is:

$$
 \Sigma^*=
\begin{pmatrix}
 \tilde\Sigma  & (\Psi\tilde\Sigma)^\top &  (\Psi^2  \tilde\Sigma)^\top & \cdots  & (\Psi^{T-1}  \tilde\Sigma)^\top\\
\Psi  \tilde\Sigma & \tilde\Sigma  &  (\Psi\tilde\Sigma)^\top  & \cdots &  (\Psi^{T-2}\tilde\Sigma)^\top\\
\Psi^2 \tilde\Sigma & \Psi \tilde\Sigma &\tilde\Sigma & \cdots &  (\Psi^{T-3} \tilde\Sigma)^\top\\
\vdots & \ddots &  \ddots & \ddots & \vdots\\
\Psi^{T-1} \tilde\Sigma & \cdots & \cdots & \Psi\tilde\Sigma & \tilde\Sigma
\end{pmatrix}.
$$

## AR(1) process with standard t errors (logit link) 

For the logit link, the situation is more complicated. If we wish to obtain a stationary 
distribution like the one above, with stationary covariance $\Sigma^*$ at a subject-level, 
we can assume standard $t$ errors on the $u_t$.
But the stationary distribution of an AR1 with t errors needs special care
(see https://arxiv.org/abs/2109.13648).

So the model is 
$$
\epsilon_t = \Psi \epsilon_{t-1} + \Sigma_t^{1/2}u_t
$$
where $u_t\sim MVT_q(0, I_q, \nu + q)$ and they are independent of $\epsilon_{t-j}, j>0$ for all $t$.
Here $\Sigma_t=\Sigma(\epsilon_{t-1})$ is heteroskedastic:
$$
\Sigma(\boldsymbol\epsilon)=\frac{\nu-2 + (\boldsymbol\epsilon - 1\otimes\mu)^\top\Sigma_0^{-1}(\boldsymbol\epsilon - 1\otimes\mu)}{\nu-2+q}\Sigma
$$
where $\mu$ should be zero in the case with no intercept and 
$vec(\tilde\Sigma)=(I_q-\Psi\otimes\Psi)^{-1}vec(\Sigma)$
Then the process $\boldsymbol\epsilon_t$ is a Markov chain with stationary distribution  
$\boldsymbol\epsilon_t\sim MVT_q(0, \tilde\Sigma,\nu)$

# Estimation using full $\Psi$
In the current
implementation the `Psi.diag` must be set to `TRUE`, as we at the
time of writing do not allow for a general structure on the matrix $\Psi$.
Setting `Psi.diag` to `FALSE` would estimate a 
general $\Psi$ matrix which satisfies the stationarity constraints of multivariate
autoregressive process. To ensure that the stationarity constraints
are satisfied, a decomposition of $\Psi$ which relies on a positive
definite matrix and an orthogonal matrix as described in Roy2019 can be employed.
For the positive definite matrix the unconstrained log matrix parameterization
can be employed while for the orthogonal matrix Givens parameterization or the 
Cayley representation can be used. 


# SEs for the error structure parameters

## $\Psi$ diagonal 

In the diagonal case with $q = 3$
we have: $\rho_1$, $\rho_2$, $\rho_3$ and 
$\xi_1$,$\xi_2$,$\xi_3$.

Consider each pair of dimensions $(k, l)$ with $k < l$ and $k, l \in \{1, 2,\cdots,qT\}$ the corresponding portion of $\Sigma^*$ is given by: 
$$
S^{(k,l)} = \begin{matrix}
 s_1 & s_2\\
 s_2 & s_3\\
 \end{matrix}
$$
where $k_t$ is the time index corresponding to the $k$-th response and  $k_j$ the outcome index corresponding to the $k$-th response.

For $q=3$ we have the following form for $\Sigma_0$
$$
\Sigma_0= \begin{pmatrix}
 \frac{1}{1-\xi_1^2} & &\\
 \frac{\rho_1}{1-\xi_1\xi_2}&  \frac{1}{1-\xi_2^2} & \\
 \frac{\rho_2}{1-\xi_1\xi_3}&\frac{\rho_3}{1-\xi_2\xi_3}& \frac{1}{1-\xi_3^2}
 \end{pmatrix}
$$


We need to compute the partial derivatives
$$
\frac{\partial S_{k,l}}{\partial \rho_{1}}, \frac{\partial S_{k,l}}{\partial \rho_{2}}, \frac{\partial S_{k,l}}{\partial \rho_{3}}, 
\frac{\partial S_{k,l}}{\partial \xi_{1}}, \frac{\partial S_{k,l}}{\partial \xi_{2}}, \frac{\partial S_{k,l}}{\partial \xi_{3}}
$$

Notice the diagonal of $\Sigma_0$. When we compute the derivative wrt to $\xi_j$
we need to take into account the derivatives
$$
\frac{\partial \ell_{1,2}}{\partial \xi_{1}}=\frac{\partial}{\partial \xi_{1}} \int_{-\infty}^{x_1/s_1}\int_{-\infty}^{x_2/s_2} f(x, y, r)dxdy=
\frac{\partial \ell_{1,2}}{\partial s_{1}}\frac{\partial s_1}{\partial \xi_{1}} + \frac{\partial \ell_{1,2}}{\partial r}\frac{\partial r}{\partial \xi_{1}}
$$
where
$$
 \ell_{1,2}=\log\Phi(x_1/s_1, x_2/s_2, r), s_1=1/\sqrt{(1-\xi_1^2)}, s_2=1/\sqrt{(1-\xi_2^2)}, r=\frac{\rho_{1,2}}{1-\xi_1\xi_2}\sqrt{(1-\xi_1^2)}\sqrt{(1-\xi_2^2)}
$$



