data {
  int<lower=1> J;
  int<lower=1> I;
  int<lower=1> T;
  int<lower=1> Kmax;
  array[J] int<lower=1> K;
  array[J] int pos_alpha_beg;
  array[J] int pos_alpha_end;
  int<lower=1> P;
  int<lower=0> N;
  array[N, J] int<lower=1, upper=Kmax> y;
  array[N] row_vector[P] x;
  array[N] int<lower=1, upper=I> firm_id;
  array[N] int<lower=1, upper=T> year_id;
}
parameters {
  vector[P] beta;
  array[J] ordered[Kmax - 1] alpha_array;
  cholesky_factor_corr[J] L_Omega;
  array[N, J] real<lower=0, upper=1> u; // nuisance that absorbs inequality constraints
  vector<lower=-1, upper=1>[J] phi;
  array[I, T] vector[J] w;
}
transformed parameters {
  array[I, T] vector[J] epsilon;
  // errors
  for (i in 1:I) {
    for (t in 1:T) {
        if (t == 1) {
          epsilon[i, t] = L_Omega * w[i, t];
        } else {
          epsilon[i, t] = diag_matrix(phi) * epsilon[i, t - 1] + L_Omega * w[i, t];
        }
    }
  }
}
model {
  L_Omega ~ lkj_corr_cholesky(4);
  to_vector(beta) ~ normal(0, 5);
  for (i in 1:I){
    for (t in 1:T) {
      for (j in 1:J) {
        w[i,t,j] ~ std_normal();
      }
    }
  }
  // likelihood
  for (n in 1 : N) {
    real mu;
    vector[J] z;
    real prev;
    mu = x[n] * beta;
    prev = 0;
    // TODO: this depends on order: firm_id should be 1,1,1,..2,2,2 and year_id 1,2,3,...1,2,3
    // if (year_id[n] == 1) {
    //   epsilon[n, ] = to_row_vector(L_Omega * w);
    // } else {
    //   epsilon[n, 1 : J] = to_row_vector(diag_matrix(phi) * to_vector(epsilon[n - 1, 1 : J]) + L_Omega * w);
    // }
    for (j in 1 : J) {
      vector[Kmax - 1] alphaj;
      real correction_ar;
      alphaj = alpha_array[j];
      if (year_id[n] == 1) {
        correction_ar = 0;
      } else {
        correction_ar = phi[j] * epsilon[firm_id[n], year_id[n] - 1, j];
      }
      // Phi and inv_Phi may overflow and / or be numerically inaccurate
      if (y[n, j] == 1) {
        real ustar1;
        real t;
        ustar1 = Phi((alphaj[1] - (mu + prev + correction_ar)) / L_Omega[j, j]);
        t = ustar1 * u[n, j];
        z[j] = inv_Phi(t);
        target += log(ustar1); // Jacobian adjustment
      } else {
        if (y[n, j] == K[j]) {
          real ustarK;
          real t;
          ustarK = Phi((alphaj[K[j] - 1] - (mu + prev + correction_ar)) / L_Omega[j, j]);
          t = ustarK + (1 - ustarK) * u[n, j];
          z[j] = inv_Phi(t);
          target += log1m(ustarK); // Jacobian adjustment
        } else {
          real ustarku;
          real ustarkl;
          real t;
          ustarkl = Phi((alphaj[y[n, j] - 1] - (mu + prev + correction_ar)) / L_Omega[j, j]);
          ustarku = Phi((alphaj[y[n, j]] - (mu + prev + correction_ar)) / L_Omega[j, j]);
          t = ustarkl + (ustarku - ustarkl) * u[n, j];
          z[j] = inv_Phi(t);
          target += log(ustarku - ustarkl); // Jacobian adjustment
        }

      }
      if (j < J) {
        prev = L_Omega[j + 1, 1 : j] * head(z, j);
      }
      // Jacobian adjustments imply z is truncated standard normal
      // thus utility --- mu + L_Omega * z --- is truncated multivariate normal
    }
  }
}
generated quantities {
  corr_matrix[J] Omega;
  vector[sum(K) - J] alpha;
  Omega = multiply_lower_tri_self_transpose(L_Omega);
  for (j in 1:J) {
    alpha[pos_alpha_beg[j]:pos_alpha_end[j]] = alpha_array[j][1 : (K[j] - 1)];
  }
}
